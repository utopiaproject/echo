package huaweicloud.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Echo extends HttpServlet{

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException,IOException {
		String queryString = req.getQueryString();
		queryString = queryString == null ? "null" : queryString;
		res.setContentType("text/plain");
		PrintWriter pw = res.getWriter();
		pw.write(queryString);
		pw.write("\n");
		pw.close();
	}
}
